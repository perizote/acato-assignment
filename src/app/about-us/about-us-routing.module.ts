import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutUsComponent } from './about-us.component';
import { dutchTeamRoutes } from './dutch-team/dutch-team.routes';
import { spanishTeamRoutes } from './spanish-team/spanish-team.routes';

const routes: Routes = [
  {
    path: 'about-us',
    component: AboutUsComponent,
    children: [
      {
        path: '',
        redirectTo: 'dutch-team',
        pathMatch: 'full'
      },
      ...dutchTeamRoutes,
      ...spanishTeamRoutes
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class AboutUsRoutingModule { }
