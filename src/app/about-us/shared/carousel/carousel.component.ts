import { Component, OnChanges, Input, SimpleChange, ElementRef, ViewChildren } from '@angular/core';

@Component({
  selector: 'carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.sass']
})
export class CarouselComponent implements OnChanges {
  @Input() carouselItems;
  @Input() shouldImageFollowMouse: boolean = false;
  @ViewChildren('teamMember') teamMembers: Array<ElementRef>;
  private isReady: boolean;

  constructor(private elementRef: ElementRef) { }

  ngOnChanges(changes: {[carouselItems: string]: SimpleChange}) {
    if (!this.carouselItems) {
      this.isReady = false;
      return;
    }
    this.isReady = this.carouselItems.length > 0;
  }

  private onMouseMove(event: MouseEvent) {
    if (!this.shouldImageFollowMouse) {
      return;
    }
    this.teamMembers
      .forEach(teamMember => this.setTeamMemberPosition(teamMember, event.clientX, event.clientY));
  }

  private setTeamMemberPosition(teamMember, mouseXPosition, mouseYPosition) {
    let width = teamMember.nativeElement.offsetWidth;
    let height = teamMember.nativeElement.offsetHeight;
    let left = teamMember.nativeElement.offsetLeft;
    let top = teamMember.nativeElement.getBoundingClientRect().top;
    let right = left + width;
    let bot = top + height;

    if (mouseXPosition < left && mouseYPosition < top) {
      teamMember.nativeElement.style.backgroundPosition = '0 0';
      return;
    }
    if (mouseXPosition > left && mouseYPosition < top && mouseXPosition < right) {
      teamMember.nativeElement.style.backgroundPosition = '-186px 0';
      return;
    }
    if (mouseYPosition < top && mouseXPosition > right) {
      teamMember.nativeElement.style.backgroundPosition = '186px 0';
      return;
    }
    if (mouseYPosition > top && mouseXPosition > right && mouseYPosition < bot) {
      teamMember.nativeElement.style.backgroundPosition = '186px';
      return;
    }
    if (mouseXPosition > right && mouseYPosition > bot) {
      teamMember.nativeElement.style.backgroundPosition = '186px 201px';
      return;
    }
    if (mouseXPosition > left && mouseYPosition > bot && mouseXPosition < right) {
      teamMember.nativeElement.style.backgroundPosition = '-186px 201px';
      return;
    }
    if (mouseXPosition < left && mouseYPosition > bot) {
      teamMember.nativeElement.style.backgroundPosition = '0 201px';
      return;
    }
    if (mouseYPosition > top && mouseXPosition < left && mouseYPosition < bot) {
      teamMember.nativeElement.style.backgroundPosition = '0';
      return;
    }
    if (mouseYPosition > top && mouseXPosition > left && mouseYPosition < bot && mouseXPosition < right) {
      teamMember.nativeElement.style.backgroundPosition = '-186px';
      return;
    }
  }

  private setBackgroundImage(carouselItem){
    return `url(${carouselItem})`;
  }
}
