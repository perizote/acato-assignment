const errors = {
  emptyName: 'Enter a name',
  emptyEmail: 'Enter an email',
  wrongEmailFormat: 'Email format is not correct'
};

export const contactFormVariables = {
  errors: errors
};
