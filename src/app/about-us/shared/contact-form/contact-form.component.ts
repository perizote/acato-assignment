import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { contactFormHelpers } from './contact-form.helpers';
import { contactFormVariables } from './contact-form-variables';
import { AboutUsService } from '../about-us.service';

@Component({
  selector: 'contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.sass']
})
export class ContactFormComponent implements OnInit {
  private form: FormGroup;
  private errors = contactFormVariables.errors;
  private responseMessage: string;
  private isReady: boolean = true;

  constructor(private formBuilder: FormBuilder, private aboutUsService: AboutUsService) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, contactFormHelpers.validateEmail]]
    });
  }

  submit() {
    if (!this.form.valid) {
      return;
    }
    this.isReady = false;
    this.aboutUsService.submitContactForm(this.form.value)
      .subscribe(
        response => {
          this.responseMessage = response.successMessage;
          this.isReady = true;
        },
        error => console.log('error', error)
      );
  }
}
