import { FormControl } from '@angular/forms';
import { contactFormVariables } from './contact-form-variables';

function validateEmail(control: FormControl) {
  let EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  if (!control.value) {
    return {
      validateEmail: {
        valid: false,
        error: contactFormVariables.errors.emptyEmail
      }
    };
  }

  return EMAIL_REGEXP.test(control.value) ? null : {
    validateEmail: {
      valid: false,
      error: contactFormVariables.errors.wrongEmailFormat
    }
  };
}

export const contactFormHelpers = {
  validateEmail: validateEmail
};
