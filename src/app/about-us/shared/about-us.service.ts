import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/Rx';
import { urlVariables } from '../../shared/url.variables';

@Injectable()
export class AboutUsService {
  constructor(private http: Http) { }

  getDutchTeamImages() {
    return this.http.get(urlVariables.urls.get.dutchTeamImages)
      .map(images => images.json());
  }

  submitContactForm(contactForm) {
    return this.http.post(urlVariables.urls.post.contact, contactForm)
      .map(responseMessage => responseMessage.json());
  }
}
