import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SpanishTeamComponent } from './spanish-team.component';

export const spanishTeamRoutes: Routes = [
  {
    path: 'spanish-team',
    component: SpanishTeamComponent
  }
];
