import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarouselModule } from '../shared/carousel/carousel.module';
import { SpanishTeamComponent } from './spanish-team.component';

@NgModule({
  imports: [
    CommonModule,
    CarouselModule
  ],
  declarations: [SpanishTeamComponent]
})
export class SpanishTeamModule { }
