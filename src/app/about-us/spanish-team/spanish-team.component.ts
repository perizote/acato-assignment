import { Component, OnInit } from '@angular/core';
import { spanishTeamVariables } from './spanish-team.variables';

@Component({
  selector: 'spanish-team',
  templateUrl: './spanish-team.component.html'
})
export class SpanishTeamComponent implements OnInit {
  private literals = spanishTeamVariables.literals;
  private spanishTeamImages = spanishTeamVariables.teamImages;

  constructor() { }

  ngOnInit() { }
}
