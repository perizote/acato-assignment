const literals = {
  title: 'SOBRE NOSOTROS',
  intro: 'Trabajamos con un equipo de apasionados de los estrategas, Scrum Master, especialistas de interacción, diseñadores, desarrolladores frontend y desarrolladores backend.'
};

const teamImages = [
  'assets/images/sergio.png',
  'assets/images/mrx.png'
];

export const spanishTeamVariables = {
  literals: literals,
  teamImages: teamImages
};
