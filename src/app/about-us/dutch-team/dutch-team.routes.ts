import { Routes } from '@angular/router';
import { DutchTeamComponent } from './dutch-team.component';

export const dutchTeamRoutes: Routes = [
  {
    path: 'dutch-team',
    component: DutchTeamComponent
  }
];
