import { Component, OnInit } from '@angular/core';
import { AboutUsService } from '../shared/about-us.service';
import { dutchTeamVariables } from './dutch-team.variables';

@Component({
  selector: 'dutch-team',
  templateUrl: './dutch-team.component.html'
})
export class DutchTeamComponent implements OnInit {
  private dutchTeamImages;
  private literals = dutchTeamVariables.literals;

  constructor(private aboutUsService: AboutUsService) { }

  ngOnInit() {
    if (this.dutchTeamImages) {
      return;
    }
    this.getDutchTeamImages();
  }

  getDutchTeamImages() {
    this.aboutUsService.getDutchTeamImages()
      .subscribe(
        images => this.dutchTeamImages = images,
        error => console.log('Error loading images', error.json()) //TODO: Manage errors with ErrorHandler
      );
  }
}
