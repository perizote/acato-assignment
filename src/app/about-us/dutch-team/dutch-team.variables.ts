const literals = {
  title: 'OVER ONS',
  intro: 'We werken met een gepassioneerd team van strategen, SCRUM masters, interactiespecialisten, ontwerpers, frontend developers en (ZEND gecertificeerde) backend developers. Maak kennis!'
};

export const dutchTeamVariables = {
  literals: literals
};
