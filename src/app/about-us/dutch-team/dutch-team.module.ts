import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarouselModule } from '../shared/carousel/carousel.module';
import { DutchTeamComponent } from './dutch-team.component';

@NgModule({
  imports: [
    CommonModule,
    CarouselModule
  ],
  declarations: [DutchTeamComponent]
})
export class DutchTeamModule { }
