import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutUsRoutingModule } from './about-us-routing.module';
import { AboutUsComponent } from './about-us.component';
import { DutchTeamModule } from './dutch-team/dutch-team.module';
import { SpanishTeamModule } from './spanish-team/spanish-team.module';
import { AboutUsService } from './shared/about-us.service';
import { ContactFormModule } from './shared/contact-form/contact-form.module';

@NgModule({
  imports: [
    CommonModule,
    AboutUsRoutingModule,
    DutchTeamModule,
    SpanishTeamModule,
    ContactFormModule
  ],
  declarations: [AboutUsComponent],
  providers: [AboutUsService]
})
export class AboutUsModule { }
