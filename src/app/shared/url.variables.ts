const host = 'http://localhost:3003/';

const urls = {
  get: {
    dutchTeamImages: `${host}dutch-team/images`
  },
  post: {
    contact: `${host}contact`
  }
};

export const urlVariables = {
  urls: urls
};
