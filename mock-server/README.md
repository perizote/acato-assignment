# Dyson Mock Server

This folder contains the dyson node server for dynamic, fake JSON.

Dyson allows you to define endpoints at a path and return JSON based on a template object. A full fake server for your application up and running in minutes.

## Launch the mock server

To start up the mock server, run (in a different terminal window):
`npm run mocks`
