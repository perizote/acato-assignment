var contact = {
    path: '/contact',
    callback: function(req, res, next) {
      res.status(200);
      res.body = {
        successMessage: 'Contact done successfully.'
      };
      next();
    },
    delay: [200, 800]
};

module.exports = [contact];
