import { AcatoAssignmentPage } from './app.po';

describe('acato-assignment App', function() {
  let page: AcatoAssignmentPage;

  beforeEach(() => {
    page = new AcatoAssignmentPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
